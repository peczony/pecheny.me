+++
title = "Alexander Pecheny"
date = "2015-01-01"
description = "Alexander Pecheny’s homepage"
+++

<figure><img id="mainlogo"></figure>
<style>
#mainlogo {
    content: url("/cat-for-light-theme.webp");
}
@media screen and (prefers-color-scheme: dark) {
    #mainlogo {
        content: url("/cat-for-dark-theme.webp");
    }
}
</style>

<div class="flex-wrapper">
<div id="links_n_stuff">
<p style="text-align: center; font-weight: bold;"><a href="blog/">блог</a> · <a href="dreams/">сны</a> · <a href="friendsdreams/">сны друзей</a> · <a href="music/">музыка</a> · <a class="scaps" href="okx/">ОКХ</a><br/>
<a href="mailto:ap@pecheny.me">почта</a> · <a href="http://last.fm/user/aflosev">ласт.фм</a> · <a href="https://twitter.com/pecheny">твиттер</a> · <a href="https://gitlab.com/peczony">гитлаб</a> · <a href="https://github.com/peczony">гитхаб</a> · <a href="https://t.me/pecheny">телеграм</a> · <a href="https://facebook.com/pecheny">фейсбук</a> · <a class="scaps" href="http://rateyourmusic.com/~aflosev">RYM</a></p>
</div>

<div id="footer"><p class="footnote center">Site is powered <a href="https://gohugo.io">by Hugo</a>, theme by me &amp; <a href="https://daletskaya.com">Arina Daletskaya</a>.</p>
<p class="footnote center">You can use <span class="scaps">RSS</span> to subscribe by adding <code>/index.xml</code> to any feed page.</p></div>
</div>