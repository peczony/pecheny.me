+++
date = "2015-05-14"
slug = "gamification-ii"
title = "Геймификация II"

+++

Очередной {{< sc "ШЧР" >}} проходит в Перми, в его рамках проводится чемпионат по кинект-игре, похожей на «Гарри Поттера». Чтобы применить в ней заклинание, нужно сложить руку определённым образом или совершить последовательность движений. Я не смог пройти даже первую миссию тьюториала, где требовалось перекрасить волосы своему персонажу из белого в тёмно-синий при помощи следующего заклинания:

![](http://i.imgur.com/b32GXK3.jpg)

Этажом ниже был другой конкурс, по запоминанию последовательностей из трёх длинных псевдослов. В нём Панама уверенно лидировал.