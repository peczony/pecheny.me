+++
date = "2016-03-18T10:00:00+03:00"
slug = "brain"
title = "Мозг"
+++

Мне подарили торт в белой глазури. Торт был в форме гроба, а в самом центре ― в форме мозга (как будто в центр гроба врезали мозг). Я его разрезал, но там вместо сладкого оказалось мясо с кровью. Ну ладно, ем мясо с кровью, вкусно даже. Дошёл до мозга, ем его и вдруг понимаю, что это мой собственный мозг ― он переместился в торт, и теперь я думаю из торта.