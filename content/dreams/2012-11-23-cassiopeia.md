+++
date = "2012-11-23"
slug = "cassiopeia"
title = "С кисточкой"

+++

<em>послушал кассиопею на ночь, приснился такой сон.</em>

я жил в двухэтажном деревянном доме со своей девушкой, это было странное создание, обладавшее восьмиметровым хоботом с кисточкой на конце и перьями на ногах. ещё там было много чгкшников, которых я постоянно звал в гости. и — апофеоз — я прыгал через гигантские ромашки, поедаемые сотнями пчёл, и в конце концов приземлился с высоты около 10 м

