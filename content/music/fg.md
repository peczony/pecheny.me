+++
title = "Дружелюбная мясорубка"
type = "fixed"
date = "2015-06-17"
+++

![](http://i.imgur.com/4nQNvdX.jpg)

<div style="text-align: right;">А вам милы лишь месорупки,<br/>Оне одне!<br>—<a href="http://mitin.com/books/shysh/marevo.shtml">Шиш Брянский</a></div>

Это — официальный сайт арт-проекта<br>
«**Friendlygrinder**»,<br>
объединяющего образцы творчества некоторых людей,<br>
так или иначе связанных с [чгк](http://community.livejournal.com/chgk/).

[Музыка](http://rutracker.org/forum/viewtopic.php?t=3730935) — записи Хиса, Панамы, Сета, «Дружелюбной мясорубки».

Тексты [Хиса](hision), [Лю](lu) и [Сета](set).