+++
date = "2015-07-24"
title = "Музыка"
description = "Музыкальные проекты, концерты и прочее"
type = "fixed"
suppressList = true
url = "/music"
datepublish = true
+++

[<i class="fa  fa-paper-plane"></i> Телеграм-канал про музыку](https://t.me/aflosev)

[Концерты](seenlive), на которых я был

Лучшая (поп-)музыка: [2012](http://rateyourmusic.com/collection/aflosev/strm_relyear,ss.rd.ed/2012), [2013](http://rateyourmusic.com/collection/aflosev/strm_relyear,ss.rd.ed/2013), [2014](http://rateyourmusic.com/collection/aflosev/strm_relyear,ss.rd.ed/2014), [2015](http://rateyourmusic.com/collection/aflosev/strm_relyear,ss.rd.ed/2015), [всех времён](http://rateyourmusic.com/collection/aflosev/strm_relyear,ss.rd.a.ed)

---

Музыкальные проекты, к которым я имел прямое или косвенное отношение:

# [Дружелюбная мясорубка](fg)

Акустический фолк.

Состав:

- Сет – вокал, гитара
- Пикси – баян
- Хис – виолончель
- Штых, Птица – скрипка

Породила некоторое количество спин-оффов, например, [Full Shalanda](http://www.youtube.com/watch?v=I4XBMGDSk60) и My Ground Friend.

# [Панама](panama)

Авторская песня. Экзистенция и алкоголизм.

# [{{% sc "МГИМБ" %}} ](mgimb)

Акустический постмодернистский панк.

# [Я сольно](https://vk.com/baraquepop)