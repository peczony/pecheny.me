+++
title = "Видел вживую"
type = "fixed"
datepublish = true
date = "2020-03-20T00:13:00+03:00"
+++

Отметил звёздочкой особо понравившиеся концерты/сеты.

{{< la "Петля Пристрастия" >}} (×7)  
{{< la "Барон" >}} (×4)  
{{< la "Псой Короленко" >}} (×4)  
{{< la "Arms and Sleepers" >}} (×2)  
{{< la "Cigarettes After Sex" >}} (×2)  
{{< la "Зарница" >}} (×2)  
{{< la "Комба Бакх" >}} (×2)  
{{< la "Монеточка" >}} (×2)  
{{< la "Псой и Опа!" >}} ⭐ (×2)  
{{< la "Созвездие Отрезок" >}} ⭐ (×2)  
{{< la "4 Позиции Бруно" >}} ⭐  
{{< la "6lack" >}}  
{{< la "7011" >}}  
{{< la "Anna of the North" >}}  
{{< la "Another Lips" >}}  
{{< la "Arcade Fire" >}} ⭐  
{{< la "Arctic Monkeys" >}}  
{{< la "Bad Zu" >}}  
{{< la "Big Russian Boss" >}}  
{{< la "Bosnian Rainbows" >}}  
{{< la "Brockhampton" >}}  
{{< la "CALLmeKAT" >}}  
{{< la "Chad VanGaalen" >}}  
{{< la "Charlotte Gainsbourg" >}}  
{{< la "Current 93" >}}  
{{< la "DaKooka" >}}  
{{< la "Fever Ray" >}}  
{{< la "Franz Ferdinand" >}}  
{{< la "Foals" >}}  
{{< la "Goran Bregović" >}}  
{{< la "Grizzly Bear" >}}  
{{< la "Human Tetris" >}}  
{{< la "IAMX" >}}  
{{< la "Interpol" >}}  
{{< la "Jay-Jay Johansson" >}}  
{{< la "Kendrick Lamar" >}}  
{{< la "King Gizzard & the Lizard Wizard" >}}  
{{< la "kommunikация" >}}  
{{< la "Kraków Loves Adana" >}}  
{{< la "LATEXFAUNA" >}} ⭐  
{{< la "Lucidvox" >}}  
{{< la "Lykke Li" >}}  
{{< la "Matt Elliott" >}} ⭐  
{{< la "nothing,nowhere." >}}  
{{< la "Oi Va Voi" >}}  
{{< la "Parov Stelar" >}}  
{{< la "Pharaoh" >}}  
{{< la "Sirotkin" >}}  
{{< la "Shame" >}}  
{{< la "Slaves" >}}  
{{< la "Souloud" >}} ⭐  
{{< la "St. Vincent" >}}  
{{< la "The Kilimanjaro Darkjazz Ensemble" >}}  
{{< la "The Real Tuesday Weld" >}}  
{{< la "Tricky" >}}  
{{< la "Vesta" >}}  
{{< la "Wild Beasts" >}}  
{{< la "Zемфира" >}}  
{{< la "Аигел" >}}  
{{< la "Арчанга" >}}  
{{< la "Аукцыон" >}}  
{{< la "Время Срать" >}}  
{{< la "Встреча Рыбы" >}}  
{{< la "Выход" >}}  
{{< la "Ежемесячные" >}}  
{{< la "Ёлочные игрушки" >}}  
{{< la "Июльские дни" >}}  
{{< la "Карл Хламкин и Огнеопасноркестр" >}}  
{{< la "Комсомольск" >}}  
{{< la "Кровосток" >}}  
{{< la "Леонид Фёдоров, Владимир Волков" >}}  
{{< la "ЛСП" >}}  
{{< la "Масло чёрного тмина" >}}  
{{< la "Мегаполис" >}}  
{{< la "Мы" >}}  
{{< la "НИИ Косметики" >}}  
{{< la "Овсянкин" >}}  
{{< la "Озёра" >}}  
{{< la "Пасош" >}}  
{{< la "Пионерлагерь Пыльная Радуга" >}}  
{{< la "Пошлая Молли" >}}  
{{< la "Птицу Емъ" >}}  
{{< la "Разбітае Сэрца Пацана" >}}  
{{< la "Ритуальные услуги" >}}  
{{< la "СБПЧ" >}}  
{{< la "Серебряная свадьба" >}}  
{{< la "Синекдоха Монток" >}}  
{{< la "Учитель труда" >}}  
{{< la "Фонтан" >}}  
{{< la "Хадн дадн" >}}  
{{< la "Хаски" >}}  
{{< la "Хоронько Оркестр" >}}  
{{< la "Церковь Детства" >}}