+++
categories = ["шрифты"]
date = "2021-10-10T22:10:00+03:00"
slug = "fonts"
title = "Рекомендации шрифтов"
type = "blog"
aliases = ["/fonts"]
+++

У меня есть обсессия — раз в пару месяцев ходить на Fontsquirrel и Google Fonts и проверять, что новенького появилось.

<!--more-->

Решил, что хочу зафиксировать рекомендации, чтобы сердце немного успокоилось, да и вдруг кому-нибудь будет полезно. Буду периодически обновлять.

Мои критерии простые — красивый, бесплатный/опенсорс, с кириллицей, хорошо читается, не бесит, знак ударения ([Combining Acute Accent U+0301](https://unicode-table.com/en/0301/)) нормально работает с кириллическими буквами.

## С засечками

![](https://i.imgur.com/RQ4ONDq.png)
[Source Serif Pro](https://fonts.google.com/specimen/Source+Serif+Pro) — нейтральный, приятный.

![](https://i.imgur.com/6KhtL5s.png)
[Bona Nova](https://fonts.google.com/specimen/Bona+Nova) — крутой олдстайл, но подойдёт не для всех текстов, его красота слишком притягивает глаз.

![](https://i.imgur.com/7lzoyqF.png)
[STIX Two Text](https://fonts.google.com/specimen/STIX+Two+Text) — по сути это улучшенная версия Times New Roman, если хочется «классики», но чтобы глаза не вытекали, берите его.

![](https://i.imgur.com/hpaHhs4.png)
[Vollkorn](https://fonts.google.com/specimen/Vollkorn) — появился раньше всех вышеперечисленных, в те годы был чуть ли не единственным нормальным опенсорс серифом и тем завоевал место в моём сердечке (именно им набран этот текст). Минусы — дефолтный weight регуляра слишком большой для современных HiDPI экранов; симметричная единица русскоязычными людьми плохо читается.

## Без засечек

![](https://i.imgur.com/eQIKbGO.png)
~~[PT Root UI](https://www.paratype.ru/fonts/pt/pt-root-ui) — украшение серии PT, отличный кириллический DIN. Минусы — нет на гугл фонтс; узость букв может негативно влиять на читабельность в маленьких кеглях.~~ [Паратайп оказались фашистами](https://twitter.com/rasskazov/status/1575125976585060352), не используйте их шрифты.

![](https://i.imgur.com/5qeFfGX.png)
[Commissioner](https://fonts.google.com/specimen/Commissioner) — широкий шрифт (то есть более читаемый, чем узкий PT Root). На гугл фонтс нет италиков (ну как италиков, это просто slanted версия), зато они есть [на гитхабе](https://github.com/kosbarts/Commissioner/releases). Есть проблемы с кернингом кириллицы.

![](https://i.imgur.com/c1xqhbh.png)
[Open Sans](https://fonts.google.com/specimen/Open+Sans) — ещё один широкий, хорошо читаемый шрифт. Субъективно кажется менее красивым, чем Комиссионер, но пока что лучше проработан. Если нужно ещё больше символов — берите сделанный на его основе [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans).

![](https://i.imgur.com/0wztpo0.png)
[Inter](https://fonts.google.com/specimen/Inter) — лучшее, что опенсорс мог предложить по приближению к великолепию [San Francisco](https://en.wikipedia.org/wiki/San_Francisco_(sans-serif_typeface)). Минус — знак ударения не работает.

## Моноширинные

![](https://i.imgur.com/4rKQf82.png)
[JetBrains Mono](https://fonts.google.com/specimen/JetBrains+Mono) — тонкий, удобный,субъективно, наверное, самый приятный моноширинник для кода.

![](https://i.imgur.com/9O6QzQW.png)
[Hack](https://github.com/source-foundry/Hack) — если нравится [Menlo](https://en.wikipedia.org/wiki/Menlo_(typeface)), но у вас не мак или просто хочется опенсорса.

![](https://i.imgur.com/ZJ438DP.png)
[iA Fonts](https://github.com/iaolo/iA-Fonts) — ненастоящие моноширинники: улучшенные командой iA Writer версии [IBM Plex Mono](https://fonts.google.com/specimen/IBM+Plex+Mono) для написания текстов. Тонский попытался приблизиться к эффекту iA, сохраняя моноширинность, в своём [Writer](https://github.com/tonsky/font-writer) (см. также [пост](https://tonsky.me/blog/sublime-writer/) для контекста).

![](https://i.imgur.com/nm5qCPJ.png)
[Курьер Прайм](http://dimkanovikov.pro/courierprime/) — «если хочется классики», улучшенный Курьер. Минус — знак ударения не работает.

![](https://i.imgur.com/Vbpoby0.png)
[Fira Code](https://fonts.google.com/specimen/Fira+Code) — улучшенная Тонским версия Fira Sans с кучей кодерских лигатур. Если кому-то нужны все лигатуры, то наверное кайф, мне эстетически Фира совсем не нравится.