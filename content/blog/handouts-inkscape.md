+++
categories = ["чгк"]
date = "2016-05-20T18:26:00+03:00"
slug = "handouts-inkscape"
title = "Подготовка раздаток в Inkscape"
type = "blog"
+++

Хороший тон для редактора — стараться облегчить организатору турнира его нелёгкий труд. Одно из мест, где это без особых усилий можно сделать — печать раздаток. Сегодня я расскажу, как передать в распоряжение организатора не голую картинку, а пдфку формата A4 с размноженной на энное количество команд картинкой.

Для этого мы воспользуемся свободной и бесплатной программой [Inkscape](https://inkscape.org/en/)[^1]. Под Windows и Linux установка тривиальна, пользователям Мака придётся поставить XQuartz, но я рассчитываю, что вы справитесь.

Итак, пусть у нас есть картинка, которую мы хотим раздать — вот она:

![](http://www.thatericalper.com/wp-content/uploads/2014/07/rick-astley.jpg)

Запускаем Inkscape, перед нами пустой лист A4. Заходим в меню _File_, кликаем _Import_, загружаем картинку.

![](http://i.imgur.com/uegQD4C.png)

Прикиньте, какого минимального размера должна быть раздатка, чтобы нужные вам детали было видно. Для этого полезно воспользоваться меню _View → Zoom → 1:1_ и приложить к экрану настоящий лист A4 — через него будет просвечивать ваша раздатка. Включите инструмент «Стрелка», если ещё не, кликните по картинке, зажмите _Ctrl_ (чтобы уменьшение сохраняло масштаб) и уменьшите её до нужного размера.

![](http://i.imgur.com/QtZt1EM.png)

Теперь выберите пункт меню _Edit → Clone → Create Tiled Clones_ и выберите справа вкладку _Shift_.

![](https://imgur.com/9qggoQU)

Чтобы получилась ровная сетка, у вас должны быть нулевыми значения _Shift X per row_ и _Shift Y per column_, и ненулевыми, соответственно, значения _Shift Y per row_ и _Shift X per column_. Подберите подходящие значения этих параметров так, чтобы организатору было удобно резать, и выберите нужное количество столбцов и строк (_Rows, columns_), чтобы заполнить лист. У вас получится что-то в этом роде:

![](http://i.imgur.com/YduGH4H.png)

Теперь осталось только удалить изначальную картинку (она при клонировании остаётся на месте, и получается две картинки одна над другой) и сохранить как пдф (_File → Save As... → PDF_).

Полезно сохранить файл под таким именем, чтобы понятно было, на сколько команд рассчитан этот лист. Для этого нужно перемножить ваши строки и столбцы и разделить на три. В моём случае выходит шесть, называем файл `rickastley_6teams.pdf` и сохраняем. Готово, вы восхитительны!

[^1]: Вообще говоря, это наверняка можно с приемлемым качеством делать автоматически, но я пока с этой задачей не справился — буду держать в курсе.